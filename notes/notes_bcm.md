# google project 0: Over The Air: Exploiting Broadcom’s Wi-Fi Stack (Part 1) (2017)
https://googleprojectzero.blogspot.com/2017/04/over-air-exploiting-broadcoms-wi-fi_4.html
https://bugs.chromium.org/p/project-zero/issues/detail?id=1051
https://bugs.chromium.org/p/project-zero/issues/detail?id=1046#c2 (with tools and exploit)

# comsecurity: Emulation and Exploration of BCM WiFi Frame Parsing using LuaQEMU
* isolate interesting part
* emulates code via LuaQEMU, skipping the non-relevant/non emulable stuff 
https://comsecuris.com/blog/posts/luaqemu_bcm_wifi/

# nathan voss: AFL-unicorn
* introduces AFL-unicorn to fuzz emulated code
https://medium.com/@njvoss299/afl-unicorn-fuzzing-arbitrary-binary-code-563ca28936bf
* targets a dummy RF receiver
* code to be fuzzed must be isolated and stripped of any dependency (need to find the exact function, i.e. parser)
* needs a full memory dump at the start of the function to be fuzzed
* needs an AFL harness which first loads the memory dump via unicorn, then start AFL as normal (testcases will be seeded to the function to be fuzzed)
https://hackernoon.com/afl-unicorn-part-2-fuzzing-the-unfuzzable-bea8de3540a5

