# Shannon baseband reversing notes
## High-end Samsung devices since S6, Exynos based (all except US devices)

# references
## REcon 2016 - Breaking Band (Nico Golde, Daniel Komaromy)
https://www.youtube.com/watch?v=o280NiZjNu8
https://comsecuris.com/slides/recon2016-breaking_band.pdf
https://github.com/Comsecuris/shannonRE

## Insomni'hack 2018 - A walk with Shannon: A walkthrough of a pwn2own baseband exploit (Amat Cama)
https://www.youtube.com/watch?v=6bpxrfB9ioo (2018), based on the Comsecuris 2016 research)

## Reversing Samsung S6 modem
http://arm.ninja/2016/03/04/reverse-engineering-samsung-s6-modem/ (old)

# target
Galaxy Note 8 (SM-N950F)
~~~
# june 2018 patch level, Italy ITV
SM-N950F_1_20180614082040_ch86hf8zkz_fac.zip
~~~

# content
* we're interested in the modem, which runs on the core processor
~~~
❯ ls -l SM-N950F_1_20180614082040_ch86hf8zkz_fac
total 4803888
# AP, runs on application processor (the actual Android image)
-rw-rw-r-- 1 valerino valerino 3762370745 giu 14 08:34 AP_N950FXXS3CRF1_CL13094739_QB18404863_REV00_user_low_ship_meta.tar.md5

# bootloader
-rw-rw-r-- 1 valerino valerino    2293937 giu 14 08:34 BL_N950FXXS3CRF1_CL13094739_QB18404863_REV00_user_low_ship.tar.md5

# CP, runs on core processor. >>>>>>>>> Shannon baseband here!
-rw-rw-r-- 1 valerino valerino   26450108 giu 14 08:39 CP_N950FXXU3CRE1_CP9610557_CL13094739_QB17995241_REV00_user_low_ship.tar.md5

# apps, OEM customization (wipe)
-rw-rw-r-- 1 valerino valerino  564029624 giu 14 08:39 CSC_OXM_N950FOXM3CRE5_CL13094739_QB18182325_REV00_user_low_ship.tar.md5

# apps, OEM customization (keep data)
-rw-rw-r-- 1 valerino valerino  564009149 giu 14 08:39 HOME_CSC_OXM_N950FOXM3CRE5_CL13094739_QB18182325_REV00_user_low_ship.tar.md5
~~~

# extract baseband files from CP partition
* untarred CP has lz4 compressed files:
~~~
-r--r--r-- 1 valerino valerino 22688401 giu 24 09:58 modem.bin.lz4
-rw-r--r-- 1 valerino valerino  5242880 giu 24 09:58 modem_debug.bin
~~~
* needs lz4: **sudo apt-get install liblz4-tool on ubuntu**

## modem_debug.bin
* has a PK header
* doesn't unzip with plain _unzip_, **sudo apt-get install bsdtar**
~~~
❯ bsdtar xvf modem_debug.bin
x const.bin
x trace.bin
~~~
* probably something to help with debug with samsung tools, leave it for now .....

## modem.bin
* holds different segments, we're interested mostly in BOOT and MAIN
* template for usage with 010 editor
	* updated from Comsecuris work for newer image format
~~~
//--------------------------------------
//--- 010 Editor v5.0 Binary Template
//
// File: Samsung TOC Template
// (C) Copyright 2015/2016 Comsecuris UG
//--------------------------------------

struct TOC_header
{
    char Name[12];
    unsigned int start;
    unsigned int load_addr;
    unsigned int size;
    unsigned int crc32;
    unsigned int id;
};

struct BOOT_img (unsigned int size)
{
    unsigned int preamble[8];
    unsigned int rev;
    unsigned int date;
    unsigned int size;
    unsigned int unk;
    char hash_magic[4];
    char sha512[64];
    char code[size-512-64-4-4*4-8*4];
    char signature[512];
};

struct MAIN_img (unsigned int size)
{
    char data[size];
    unsigned int unk1;
    char padding[12];
    char signature[256];
};

SetForeColor( cRed);
TOC_header toc_hdr;
TOC_header boot_hdr;
TOC_header main_hdr;
TOC_header vss;
TOC_header nv;
TOC_header offset;

FSeek(toc_hdr.size);
SetForeColor(cBlue);
BOOT_img boot(boot_hdr.size);

FSeek(main_hdr.start);
SetForeColor(cGreen);
MAIN_img main(main_hdr.size);
~~~

### dump modem.bin segments to separate files using infos from the TOC headers (start offset, size)
~~~
dd if=./modem.bin of=./boot.bin bs=1 skip=start_offset count=size
dd if=./modem.bin of=./main.bin bs=1 skip=start_offset count=size
dd if=./modem.bin of=./vss.bin bs=1 skip=start_offset count=size
# skip NV ram 
dd if=./modem.bin of=./offset.bin bs=1 skip=start_offset count=size
~~~
* check crc32 of each after dump, must match the crc32 in TOC headers (boot, main, vss)

# load main.bin in ida at the load address stated in the TOC header
* ARM, 32bit, little endian
* segment type=RAM
* RAM size = 0x0fa00000 (256MB, arbitrary value) 
* address = 0x40010000
* select all -> c -> analyze (takes a lot of time)
* reverse :)

# THE FOLLOWING IS DEPRECATED (PRE-SAMSUNG S7)
~~~
### main.bin is encrypted :( easiest way is to take a ramdump from a live device, then map addresses reversing /sbin/cbd
* *#9900# to access service menu (each time)
* enable **HIGH debugging level** and **CP ram logging** (device will reboot at each)
* select **Run forced CP crash dump**, after a while device will reboot in crash-dump mode
* power off then on, from service menu select **Copy to SD Card including CP ram dump**
~~~
# pull dump from device
mkdir ramdump && cd ramdump
adb pull /sdcard/log
~~~

# extract /sbin/cbd from AP partition
~~~
# extract the android image
❯ tar -xvf ../SM-N950F_1_20180614082040_ch86hf8zkz_fac/AP_N950FXXS3CRF1_CL13094739_QB18404863_REV00_user_low_ship_meta.tar.md5
boot.img.lz4
recovery.img.lz4
system.img.lz4
userdata.img.lz4
dqmdbg.img.lz4
meta-data/
meta-data/fota.zip

# unpack /boot
❯ lz4 boot.img.lz4
Decoding file boot.img.lz4
Successfully decoded 36663584 bytes

# need this tool to extract the boot image
❯ git clone https://github.com/csimmonds/boot-extract
Cloning into 'boot-extract'...
remote: Counting objects: 34, done.
remote: Total 34 (delta 0), reused 0 (delta 0), pack-reused 34
Unpacking objects: 100% (34/34), done.

~/repos
❯ cd boot-extract

~/repos/boot-extract master
❯ make
cc -Wall    boot-extract.c   -o boot-extract

# finally extract boot image
mkdir boot && cd boot

~/research/baseband/firmwares/galaxy-note8/ap/boot
❯ ~/repos/boot-extract/boot-extract ../boot.img
argc 2 optind 1
Boot header
  flash page size       2048
  kernel size           0x1cbbcf0
  kernel load addr      0x10008000
  ramdisk size          0x553aef
  ramdisk load addr     0x11000000
  second size           0x0
  second load addr      0x10f00000
  tags addr             0x10000100
  product name          'SRPQC03B001KU'
  kernel cmdline        ''

zImage extracted
ramdisk offset 30132224 (0x1cbc800)
ramdisk.cpio.gz extracted

~/research/baseband/firmwares/galaxy-note8/ap/boot
❯ zcat ./ramdisk.cpio.gz | cpio -i
20650 blocks
~~~

## we finally have sbin/cbd, the core processor boot daemon
~~~
❯ file sbin/cbd
sbin/cbd: ELF 64-bit LSB executable, ARM aarch64, version 1 (SYSV), statically linked, BuildID[md5/uuid]=5ce579676fa9708f2849859d34799502, stripped
~~~
* loading this in IDA, we should be able to find how the different modem.bin parts are laid out in memory
~~~


