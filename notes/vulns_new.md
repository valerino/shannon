# smsDecodeCellInfoRspMsg()
## heap overflow ?
~~~
void __fastcall sms_DecodeMmCellInfoRspMsg(int a1)
{
  int v1; // r0
  int v2; // r3
  VDBT_struct *v3; // r1
  VDBT_struct *v4; // r2
  int v5; // r0
  VDBT_struct *v6; // [sp+0h] [bp-28h]
  int v7; // [sp+4h] [bp-24h]
  __int64 ptrToDbt; // [sp+10h] [bp-18h]

  v1 = sub_415C2720(a1);
  v3 = (VDBT_struct *)0xFECDBA98;
  v4 = (VDBT_struct *)0x40585;
  if ( v1 == 1 || v1 == 2 )
  {
    if ( BYTE1(smsStateStruct) < 2u )
    {
      LODWORD(ptrToDbt) = &DBT_PLMN_ID__x__x__x__LAC____x__x__CELL_ID_0x_x;
      HIDWORD(ptrToDbt) = (BYTE1(smsStateStruct) << 0x12) + 0x40585;
      palLog(
        &ptrToDbt,
        MEMORY[1],
        MEMORY[2],
        MEMORY[3],
        MEMORY[4],
        MEMORY[5],
        MEMORY[8],
        0xFECDBA98,
        __PAIR__(HIDWORD(ptrToDbt), &DBT_PLMN_ID__x__x__x__LAC____x__x__CELL_ID_0x_x));
    }
    v5 = 0x18 * BYTE1(smsStateStruct) + 0x433255B4;
    *(_WORD *)(v5 + 4) = MEMORY[1];
    *(_BYTE *)(v5 + 6) = MEMORY[3];
    *(_WORD *)(v5 + 7) = MEMORY[4];
    v3 = (VDBT_struct *)MEMORY[8];
    *(_DWORD *)(v5 + 0xC) = MEMORY[8];
  }
  else if ( BYTE1(smsStateStruct) < 2u )
  {
    v6 = &DBT_MmCellInfoRsp_ignored;
    v7 = (BYTE1(smsStateStruct) << 0x12) + 0x40585;
    palLog(&v6, 0xFECDBA98);
  }
  
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  vsub_415BF5D4(2, v3, v4, v2);**
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
}

void __fastcall vsub_415BF5D4(int a1, VDBT_struct *a2, VDBT_struct *a3, int a4)
{
  int v4; // r6
  int v5; // r8
  VDBT_struct *v6; // r6
  int v7; // r3
  unsigned int i; // r5
  int v9; // r4
  int v10; // r4
  VDBT_struct *v11; // r2
  int v12; // r3
  int v13; // r0
  VDBT_struct *v14; // [sp+0h] [bp-28h]
  VDBT_struct *v15; // [sp+4h] [bp-24h]
  int v16; // [sp+8h] [bp-20h]

  v14 = a2;
  v15 = a3;
  v16 = a4;
  v4 = a1;
  v5 = sub_415C2720(a1);
  logCall(0x404C526B);
  if ( v5 == 1 )
  {
    if ( !*(_BYTE *)(0x98 * BYTE1(smsStateStruct) + 0x43323FE9)
      && sub_415C0956(
           *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FE2),
           (unsigned __int16)(*(_WORD *)(0x98 * BYTE1(smsStateStruct) + 0x43323FE4) + 0xF),
           *(unsigned __int8 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FEB),
           v4) == 1 )
    {
      sub_415C0BBE(
        *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FE2),
        *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FE4));
    }
    if ( sub_415C0956(
           *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FE2),
           *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FE4),
           *(unsigned __int8 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FEB),
           2) == 1 )
    {
      if ( BYTE1(smsStateStruct) < 2u )
      {
        v14 = &DBT_Duplicate_CBS_received__returning__sms_CbBlock1Type;
        v15 = (VDBT_struct *)((BYTE1(smsStateStruct) << 0x12) + 0x405A1);
        palLog(&v14, *(unsigned __int8 *)(0x98 * BYTE1(smsStateStruct) + 0x43324069), 0xFECDBA98);
      }
    }
    else
    {
      v6 = (VDBT_struct *)(unsigned __int8)(0x16 * *(_BYTE *)(0x98 * BYTE1(smsStateStruct) + 0x43324010));
      if ( (unsigned int)&v6[0xFFFFFFFF].field_4 + 2 > 0x42 )
      {
        if ( BYTE1(smsStateStruct) < 2u )
        {
          v14 = &DBT_Length_Error_can_t_send_CB_Msg;
          v15 = (VDBT_struct *)((BYTE1(smsStateStruct) << 0x12) + 0x405A1);
          palLog(&v14, 0xFECDBA98);
        }
      }
      else
      {
        if ( BYTE1(smsStateStruct) < 2u )
        {
          v14 = &DBT_New_CBS_is_received__Add_to_DB_and_send_CB_IND;
          v15 = (VDBT_struct *)((BYTE1(smsStateStruct) << 0x12) + 0x405A1);
          palLog(&v14, 0xFECDBA98);
        }
        sub_415C0866(
          *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FE2),
          *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FE4),
          *(unsigned __int8 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FEB),
          2);
        
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		sub_415BF2B0(0x98 * BYTE1(smsStateStruct) + 0x43323FE0, v6, 0x98 * BYTE1(smsStateStruct) + 0x43324011, v7);
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      }
    }
    sub_415BAE44(0x98 * BYTE1(smsStateStruct) + 0x43323FF8);
    sub_415BAE44(0x98 * BYTE1(smsStateStruct) + 0x43323FE0);
    *(_BYTE *)(0x98 * BYTE1(smsStateStruct) + 0x4332406A) = 1;
  }
  else if ( v5 == 2 || v5 == 4 )
  {
    if ( !*(_BYTE *)(0x98 * BYTE1(smsStateStruct) + 0x43324001)
      && sub_415C0956(
           *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FFA),
           (unsigned __int16)(*(_WORD *)(0x98 * BYTE1(smsStateStruct) + 0x43323FFC) + 0xF),
           0,
           v4) == 1 )
    {
      sub_415C0BBE(
        *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FFA),
        *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FFC));
    }
    if ( sub_415C0956(
           *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FFA),
           *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FFC),
           0,
           v4) == 1 )
    {
      if ( BYTE1(smsStateStruct) < 2u )
      {
        v14 = &DBT_Duplicate_CBS_received__returning;
        v15 = (VDBT_struct *)((BYTE1(smsStateStruct) << 0x12) + 0x405A1);
        palLog(&v14, 0xFECDBA98);
      }
    }
    else
    {
      if ( BYTE1(smsStateStruct) < 2u )
      {
        v14 = &DBT_New_CBS_is_received__Add_to_DB_and_sed_CB_IND;
        v15 = (VDBT_struct *)((BYTE1(smsStateStruct) << 0x12) + 0x405A1);
        palLog(&v14, 0xFECDBA98);
      }
      sub_415C0866(
        *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FFA),
        *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FFC),
        0,
        v4);
      for ( i = 0; *(unsigned __int8 *)(0x98 * BYTE1(smsStateStruct) + 0x43324004) > i; i = (unsigned __int8)i )
      {
        v12 = *(_DWORD *)(0x98 * BYTE1(smsStateStruct) + 0x43324008);
        if ( !*(_BYTE *)(v12 + 0x52) )
        {
          break;
        }
        v9 = 0x53 * (signed __int16)i;
        LOBYTE(i) = i + 1;
        *(_BYTE *)(0x98 * BYTE1(smsStateStruct) + 0x43324003) = i;
        v10 = v9 + v12;
        if ( *((_BYTE *)&dword_41A208D8 + BYTE1(smsStateStruct) + 2) == 1 )
        {
          if ( BYTE1(smsStateStruct) < 2u )
          {
            v14 = &DBT_Data_Type____SMS_CB_UBMC_DATA_MODE;
            v15 = (VDBT_struct *)((BYTE1(smsStateStruct) << 0x12) + 0x405A5);
            palLog(&v14, 0xFECDBA98);
            if ( BYTE1(smsStateStruct) < 2u )
            {
              v14 = (VDBT_struct *)0xFECDBA98;
              v15 = &DBT_MessageIdentifer_____04d__SerialNumber_____02d__DataCodingScheme_____02d_;
              v16 = (BYTE1(smsStateStruct) << 0x12) + 0x405A5;
              palLog(
                &v15,
                *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FFA),
                *(unsigned __int16 *)(0x98 * BYTE1(smsStateStruct) + 0x43323FFC),
                *(unsigned __int8 *)(0x98 * BYTE1(smsStateStruct) + 0x43324002),
                0xFECDBA98,
                &DBT_MessageIdentifer_____04d__SerialNumber_____02d__DataCodingScheme_____02d_,
                v16);
              if ( BYTE1(smsStateStruct) < 2u )
              {
                v14 = &DBT_sms_ProcessUbmcDataIndMsg_in__s_state__CbStr__;
                v15 = (VDBT_struct *)((BYTE1(smsStateStruct) << 0x12) + 0x405A5);
                v13 = sub_415BA622(*(unsigned __int8 *)(0x98 * BYTE1(smsStateStruct) + 0x4332406A));
                palLog(&v14, v13, 0xFECDBA98);
              }
            }
          }
          somememcpyloop_checked_2(v10, *(unsigned __int8 *)(v10 + 0x52), 0x2D);
        }
        else
        {
          v11 = (VDBT_struct *)*(unsigned __int8 *)(0x98 * BYTE1(smsStateStruct) + 0x4332400C);
          v14 = 0;
          v15 = v11;
          vsub_415BF016(
            0,
            0x98 * BYTE1(smsStateStruct) + 0x43323FF8,
            *(unsigned __int8 *)(v10 + 0x52),
            v10,
            0,
            (char)v11);
        }
      }
    }
    sub_415BAE44(0x98 * BYTE1(smsStateStruct) + 0x43323FF8);
  }
  else if ( BYTE1(smsStateStruct) < 2u )
  {
    v14 = &DBT_Wrong_RAT_in_sms_SendCbInd;
    v15 = (VDBT_struct *)((BYTE1(smsStateStruct) << 0x12) + 0x405A1);
    palLog(&v14, 0xFECDBA98);
  }
}

// write access to const memory has been detected, the output may be wrong!
int __fastcall sub_415BF2B0(int a1, VDBT_struct *a2, int a3, int a4)
{
  int v4; // r5
  int v5; // r9
  VDBT_struct *v6; // r8
  int v7; // r0
  unsigned __int16 v8; // r6
  int result; // r0
  int v10; // r0
  int (__fastcall *v11)(_DWORD); // r0
  signed int v12; // r0
  int v13; // r1
  VDBT_struct *v14; // r2
  int v15; // r3
  VDBT_struct *v16; // [sp+0h] [bp-30h]
  int v17; // [sp+4h] [bp-2Ch]
  int v18; // [sp+8h] [bp-28h]

  v16 = a2;
  v17 = a3;
  v18 = a4;
  v4 = a1;
  v5 = a3;
  v6 = a2;
  if ( BYTE1(smsStateStruct) < 2u )
  {
    v16 = &DBT_sms_SendGsmSmsCbInd_in__s_state;
    v17 = (BYTE1(smsStateStruct) << 0x12) + 0x405A2;
    v7 = sub_415BA622(*(unsigned __int8 *)(0x98 * BYTE1(smsStateStruct) + 0x4332406A));
    palLog(&v16, v7, 0xFECDBA98);
  }
  v8 = (_WORD)v6 - 6;
  if ( sub_415BF276(*(unsigned __int16 *)(v4 + 2)) )
  {
    off_41A20AF4 = &off_404C5D54[3];
    sub_415C877A(0, v6);
    sub_415C87B2(1);
    result = sub_415C7686(0, 0x97, "../../../PSS/StackService/SMS/Code/Src/sms_CellBroadcast.c", 0x689, v16, v17);
    if ( result )
    {
      v10 = *(unsigned __int16 *)(v4 + 2) - 0x3E9;
      if ( *(_WORD *)(v4 + 2) == 0x3E9 )
      {
        MEMORY[4] = 1;
      }
      else
      {
        if ( v10 == 1 )
        {
          LOBYTE(v10) = 3;
LABEL_11:
          MEMORY[4] = v10;
          goto LABEL_12;
        }
        if ( v10 == 2 )
        {
          goto LABEL_11;
        }
      }
LABEL_12:
      if ( BYTE1(smsStateStruct) < 2u )
      {
        v16 = &DBT__Length_of_Broadcast_Data_Sent__;
        v17 = (BYTE1(smsStateStruct) << 0x12) + 0x405A4;
        palLog(&v16, v6, 0xFECDBA98);
      }
  	  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      mallocWrapperProbably(
        3,
        (int)&v6[0xFFFFFFFF].field_14 + 2,
        (int)"../../../PSS/StackService/SMS/Code/Src/sms_CellBroadcast.c",
        0x69C);
      MEMORY[0] = v11;
      if ( v11 )
      {
        memcpy3(v11, v5 + 6, v8);
      }
  	  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      else if ( BYTE1(smsStateStruct) < 2u )
      {
        v16 = &DBT_Memory_allocation_fails_for_sms_PtrBodySnd__SmsLcsmAsstdata_buf_ptr_;
        v17 = ((BYTE1(smsStateStruct) + 1) << 0x12) | 0x5A0;
        palLog(&v16, 0xFECDBA98);
      }
      v12 = 0x7E;
      return sub_415C8374(v12, 0);
    }
  }
  else
  {
    off_41A20AF4 = &dword_404C5CDC;
    sub_415C877A(0, v6);
    sub_415C87B2(1);
    result = sub_415C7686(0, 0x97, "../../../PSS/StackService/SMS/Code/Src/sms_CellBroadcast.c", 0x6B8, v16, v17);
    if ( result )
    {
      v18 = sub_415BB8A0(*(unsigned __int16 *)(v4 + 2), *(unsigned __int16 *)(v4 + 6));
      MEMORY[0xC] = 1;
      MEMORY[6] = *(_WORD *)v4;
      MEMORY[4] = *(_WORD *)(v4 + 2);
      MEMORY[0xF] = *(_BYTE *)(v4 + 8);
      MEMORY[0x10] = *(_BYTE *)(v4 + 9);
      MEMORY[0xE] = *(_BYTE *)(v4 + 0xA);
      MEMORY[0x11] = *(_BYTE *)(v4 + 0xC);
      MEMORY[0x12] = *(_BYTE *)(v4 + 0xB);
      MEMORY[8] = sms_GetSeparateDataBufferPtr(0, v13, v14, v15);
      LOWORD(MEMORY[0]) = (_WORD)v6 - 6;
      MEMORY[0x18] = *(_WORD *)(v4 + 6);
      MEMORY[0x1A] = v18;
      memcpy3(MEMORY[8], v5 + 6, v8);
      if ( BYTE1(smsStateStruct) < 2u )
      {
        v16 = &DBT___MsgRatMode_____02x;
        v17 = (BYTE1(smsStateStruct) << 0x12) + 0x405A4;
        palLog(&v16, MEMORY[0xC], 0xFECDBA98);
        if ( BYTE1(smsStateStruct) < 2u )
        {
          v16 = &DBT___MsgCode_____02x;
          v17 = (BYTE1(smsStateStruct) << 0x12) + 0x405A4;
          palLog(&v16, MEMORY[6], 0xFECDBA98);
          if ( BYTE1(smsStateStruct) < 2u )
          {
            v16 = &DBT___MsgId_____02x;
            v17 = (BYTE1(smsStateStruct) << 0x12) + 0x405A4;
            palLog(&v16, MEMORY[4], 0xFECDBA98);
            if ( BYTE1(smsStateStruct) < 2u )
            {
              v16 = &DBT___GeographicalArea_____02x;
              v17 = (BYTE1(smsStateStruct) << 0x12) + 0x405A4;
              palLog(&v16, MEMORY[0xF], 0xFECDBA98);
              if ( BYTE1(smsStateStruct) < 2u )
              {
                v16 = &DBT___UpdateNumber_____02x;
                v17 = (BYTE1(smsStateStruct) << 0x12) + 0x405A4;
                palLog(&v16, MEMORY[0x10], 0xFECDBA98);
                if ( BYTE1(smsStateStruct) < 2u )
                {
                  v16 = &DBT___DataCodingScheme_____02x;
                  v17 = (BYTE1(smsStateStruct) << 0x12) + 0x405A4;
                  palLog(&v16, MEMORY[0xE], 0xFECDBA98);
                  if ( BYTE1(smsStateStruct) < 2u )
                  {
                    v16 = &DBT___TotalPages_____02x;
                    v17 = (BYTE1(smsStateStruct) << 0x12) + 0x405A4;
                    palLog(&v16, MEMORY[0x11], 0xFECDBA98);
                    if ( BYTE1(smsStateStruct) < 2u )
                    {
                      v16 = &DBT___PageNumber_____02x;
                      v17 = (BYTE1(smsStateStruct) << 0x12) + 0x405A4;
                      palLog(&v16, MEMORY[0x12], 0xFECDBA98);
                      if ( BYTE1(smsStateStruct) < 2u )
                      {
                        v16 = &DBT___CbStrLength_____02x;
                        v17 = (BYTE1(smsStateStruct) << 0x12) + 0x405A4;
                        palLog(&v16, (unsigned __int16)MEMORY[0], 0xFECDBA98);
                        if ( BYTE1(smsStateStruct) < 2u )
                        {
                          v16 = &DBT___Language_____02x;
                          v17 = (BYTE1(smsStateStruct) << 0x12) + 0x405A4;
                          palLog(&v16, MEMORY[0x18], 0xFECDBA98);
                          if ( BYTE1(smsStateStruct) < 2u )
                          {
                            v16 = &DBT___CbMsgType_____02x;
                            v17 = (BYTE1(smsStateStruct) << 0x12) + 0x405A4;
                            palLog(&v16, v18, 0xFECDBA98);
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      somememcpyloop_checked_2(MEMORY[8], v8, 0x2D);
      v12 = 0x97;
      return sub_415C8374(v12, 0);
    }
  }
  return result;
}
~~~

# sm_decode_SM______RADIO_MSG__PDP_ACTIVATION_ACCEPT()
## global struct overflow ?
~~~
void sm_decode_SM______RADIO_MSG__PDP_ACTIVATION_ACCEPT()
{
  int v0; // r9
  unsigned int v1; // r4
  char v2; // r5
  int v3; // r0
  int v4; // r4
  int v5; // r0
  int v6; // r0
  __int64 v7; // r0
  int v8; // r0
  unsigned int v9; // r0
  char v10; // r0
  int v11; // r7
  int v12; // r0
  unsigned int v13; // r0
  unsigned __int8 *v14; // r7
  _DWORD *v15; // r0
  signed int v16; // r1
  signed int v17; // r0
  int v18; // r10
  unsigned int v19; // r7
  int v20; // r0
  int v21; // r7
  int v22; // r0
  bool v23; // zf
  signed int v24; // r1
  int v25; // r0
  int v26; // r0
  int v27; // r0
  const char *v28; // r0
  int v29; // r0
  int v30; // r0
  const char *v31; // r0
  int v32; // r2
  int v33; // r3
  const char *v34; // r0
  int v35; // r2
  int v36; // r3
  unsigned int v37; // r5
  int v38; // r2
  int v39; // r3
  int v40; // r1
  VDBT_struct *v41; // r2
  int v42; // r3
  _DWORD *src; // [sp+0h] [bp-D8h]
  unsigned __int8 v44; // [sp+4h] [bp-D4h]
  char apnbuf; // [sp+8h] [bp-D0h]
  unsigned __int8 v46; // [sp+6Ch] [bp-6Ch]
  unsigned __int8 v47; // [sp+70h] [bp-68h]
  unsigned __int8 v48; // [sp+74h] [bp-64h]
  unsigned __int8 size; // [sp+78h] [bp-60h]
  unsigned __int8 v50; // [sp+7Ch] [bp-5Ch]
  __int64 ptrToDbt; // [sp+80h] [bp-58h]
  unsigned __int8 v52; // [sp+88h] [bp-50h]
  unsigned __int8 v53; // [sp+8Ch] [bp-4Ch]
  int v54; // [sp+90h] [bp-48h]
  char v55; // [sp+94h] [bp-44h]
  unsigned __int8 v56; // [sp+98h] [bp-40h]
  unsigned __int8 v57; // [sp+9Ch] [bp-3Ch]
  char v58; // [sp+A0h] [bp-38h]
  char v59; // [sp+A4h] [bp-34h]
  char v60; // [sp+A8h] [bp-30h]
  char v61; // [sp+ACh] [bp-2Ch]
  char v62; // [sp+B0h] [bp-28h]

  v0 = 0xFF;
  size = 0;
  logCall_0("sm_DecodePdpActivationAcceptMsg");
  v1 = *((unsigned __int8 *)MEMORY[0] + 4);
  v2 = *((_BYTE *)MEMORY[0] + 5);
  sm_log_TiValues(&v44, v1, *((unsigned __int8 *)MEMORY[0] + 5));
  if ( v3 != 1 )
  {
    uninteresting_33(v1, v2, &v50, &v59);
    LODWORD(ptrToDbt) = &DBT_Unknown_PdTi___x___in_sm_DecodePdpActivationAcceptMsg_;
    HIDWORD(ptrToDbt) = 0x40521;
    palLog(&ptrToDbt, v50, 0xFECDBA98);
    v37 = return0_11();
    sm_SetTi(v37, v50);
    sm_SetTiOriginator(v37, (v1 & 0x80) == 0);
    sm_SetExtendedTiUsed(v37, v59);
    uninteresting_62(v37, 0x51, v38, v39);
    if ( v37 < 0xB )
    {
      init_something(v37, v40, v41, v42);
    }
    return;
  }
  sm_GetState(v44, &v60);
  sm_DisplaySessionState(v44);
  if ( v60 != 2 && v60 != 3 )
  {
    sm_SetSmCause(v44, 0x62);
    LODWORD(ptrToDbt) = &DBT_Incorrect_SM_State_in_sm_DecodePdpActivationAcceptMsg__Sending_STATUS_MSG__Cause_____s__;
    HIDWORD(ptrToDbt) = 0x40521;
    v34 = display_some_error_0(0x65u);
    palLog(&ptrToDbt, v34, 0xFECDBA98);
    uninteresting_62(v44, 0x62, v35, v36);
    return;
  }
  sm_StopTimer(v44, 0);
  v4 = 0;
  v5 = sm_GetNegotiatedQosPtr(v44, &v61);
  if ( sub_41559BD2(v5) != 1 )
  {
    LODWORD(ptrToDbt) = &DBT_sm_CheckQosIe_Err_;
    HIDWORD(ptrToDbt) = 0x40521;
    palLog(&ptrToDbt, 0xFECDBA98);
    v55 = 0;
LABEL_49:
    v4 = 0x25;
    goto LABEL_16;
  }
  v6 = getint_30();
  sm_CheckAndStoreNegotiatedQos(v44, v6, &v55, &v62);
  sm_GetModifiedQosPtr(v44, &v54);
  if ( v55 != 1 )
  {
    goto LABEL_49;
  }
  HIDWORD(v7) = 0x40524;
  LODWORD(v7) = &DBT_Qos_Accepted_in_sm_DecodePdpActivationAcceptMsg_;
  ptrToDbt = v7;
  palLog(&ptrToDbt, 0xFECDBA98);
  v8 = sm_GetNegotiatedQosPtr(v44, &v54);
  if ( sm_get_ptr_0(v8) != 2 && sub_4154D6FE() != 1 || (v9 = sub_41559BDE(), v9 <= 3) )
  {
    v9 = sm_MapPreRel99ToRel99QosParameters(v54);
  }
  if ( sub_41559C28(v9) == 1 )
  {
    src = (_DWORD *)sub_41559C12();
    if ( sub_4155BBE4(src) == 1 )
    {
      sm_SetLlcSapi(v44, src, 1);
      LODWORD(ptrToDbt) = &DBT_LlcSapi_____d_;
      HIDWORD(ptrToDbt) = 0x40522;
      palLog(&ptrToDbt, src, 0xFECDBA98);
      if ( sub_41559CC2() == 1 )
      {
        v10 = sub_41559CBA();
        v11 = sub_4155BC20(v10 & 7);
        sm_SetRadioPriority(v44, v11);
        v12 = sm_DisplayPriorityLevel(v11);
      }
      else
      {
        LODWORD(ptrToDbt) = &DBT_sm_CheckRadioPriorityIe_Err_;
        HIDWORD(ptrToDbt) = 0x40521;
        palLog(&ptrToDbt, 0xFECDBA98);
      }
      if ( sub_41559BB2(v12) != 1 )
      {
        sm_GetPdpAddressLength(v44, &v46, 0);
        if ( !v46 )
        {
          LODWORD(ptrToDbt) = &DBT_User_Requested_Dynamic_PDP__but_Network_Sent_No_PDP_Address____Rejecting_PDP_Activation_;
          HIDWORD(ptrToDbt) = 0x40521;
          palLog(&ptrToDbt);
          goto LABEL_49;
        }
        goto LABEL_47;
      }
      v13 = sub_41559BBE();
      v46 = v13;
      if ( (signed int)(v13 - 2) <= 0x14 )
      {
        if ( v13 >= 2 )
        {
          v46 = v13 - 2;
          v14 = (unsigned __int8 *)sm_get_address();
          v4 = sm_DetermineAndSetPdpType(v44, *v14, v14[1]);
          if ( v4 )
          {
            goto LABEL_16;
          }
          if ( v46 )
          {
            sm_GetPdpType(v44, (int)&v47, 1);
            if ( v47 == 0x8D )
            {
              mallocWrapperProbably(4, 0xC, (int)"../../../HEDGE/NASL3/SM/Code/Src/sm_PdpContextActivation.c", 0x500);
              src = v15;
              if ( !v15 )
              {
                LODWORD(ptrToDbt) = &DBT_Memory_allocation_failed____returning_;
                HIDWORD(ptrToDbt) = 0x40521;
                palLog(&ptrToDbt, 0xFECDBA98);
                return;
              }
              *v15 = 0;
              v15[1] = 0;
              v15[2] = 0;
              *v15 = *(_DWORD *)(v14 + 2);
              v15[1] = *(_DWORD *)(v14 + 0xE);
              v15[2] = *(_DWORD *)(v14 + 0x12);
              sm_SetPdpAddress(v44, (int)v15, 0xC, 1);
              sm_SetPdpAddressLength(v44, 0xC, 1);
              LODWORD(ptrToDbt) = &DBT_Modified_Pdp_Address________;
              HIDWORD(ptrToDbt) = 0x40522;
              palLog(&ptrToDbt, v46, 0xFECDBA98);
              hexdump_something((int)src, 0xC, 5);
              memfreeinternal((int)&src);
            }
            else if ( v47 == 0x57 )
            {
              sm_SetPdpAddress(v44, (int)(v14 + 0xA), 8, 1);
              sm_SetPdpAddressLength(v44, 8, 1);
              LODWORD(ptrToDbt) = &DBT_changed_Pdp_Address________;
              HIDWORD(ptrToDbt) = 0x40522;
              palLog(&ptrToDbt, v46, 0xFECDBA98);
              hexdump_something((int)(v14 + 0xA), 8, 5);
            }
            else
            {
              if ( v47 == 0x21 )
              {
                sm_SetPdpAddress(v44, (int)(v14 + 2), 4, 1);
                v16 = 4;
              }
              else
              {
                LODWORD(ptrToDbt) = &DBT_Unknown_Pdp_Address_Length__d__;
                HIDWORD(ptrToDbt) = 0x40522;
                palLog(&ptrToDbt, v46, 0xFECDBA98);
                v16 = 0;
              }
              sm_SetPdpAddressLength(v44, v16, 1);
            }
            if ( v46 <= 0x14u )
            {
              LODWORD(ptrToDbt) = &DBT_Pdp_Address_______Len__d_;
              HIDWORD(ptrToDbt) = 0x40522;
              palLog(&ptrToDbt);
              hexdump_something((int)(v14 + 2), v46, 5);
            }
          }
LABEL_47:
          v17 = uninteresting_63();
          if ( v17 == 1 )
          {
            v18 = getint_27();
            v19 = sm_getCfgOptsLength(v18);
            if ( v19 > 0xFB )
            {
              v19 = 0;
            }
            sm_SetProtConfigOptsLength(v44, v19, 1);
            LODWORD(ptrToDbt) = &DBT_Prot_Cfg_Opts_Length_____d___;
            HIDWORD(ptrToDbt) = 0x40522;
            palLog(&ptrToDbt, v19, 0xFECDBA98);
            if ( v19 )
            {
              sm_SetProtConfigOpts(v44, v18, v19, 1);
              LODWORD(ptrToDbt) = &DBT_Protocol_Config__Options________;
              HIDWORD(ptrToDbt) = 0x40522;
              palLog(&ptrToDbt, 0xFECDBA98);
              hexdump_something(v18, v19, 2);
            }
          }
          v20 = sub_41559C68(v17);
          if ( v20 == 1 )
          {
            v21 = sub_41559C60();
            sm_SetPcktFlowId(v44, v21);
            v20 = sm_DisplayPacketFlowId(v21);
          }
          if ( sub_41559C80(v20) == 1 )
          {
            v0 = sub_41559C74();
  			// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            sm_getApnLength(v44, (int)&size, 1);
            if ( size )
            {
              sm_GetApn(v44, (int)&apnbuf, 1, size);
            }
            call_memcpy_new_unchecked_size(v0, (int)&apnbuf, size);
  			// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            display_some_error(v0);
          }
          sm_SetSmCause2ForDualPDP(v44, v0);
          sm_GetPdpType(v44, (int)&v48, 0);
          sm_GetPdpType(v44, (int)&v47, 1);
          LODWORD(ptrToDbt) = &DBT_Cause___0x_x__PDPtype__0x_x_SendPdpType_0x_x_;
          HIDWORD(ptrToDbt) = 0x40522;
          src = (_DWORD *)0xFECDBA98;
          palLog(&ptrToDbt, v0, v47, v48, 0xFECDBA98);
          if ( v0 != 0x34 && (sub_40CFF59E(v22) == 1 || v0 != 0xFF) || v48 != 0x8D || v47 == 0x8D )
          {
            v23 = MEMORY[0x42E22B7C] == 1;
            if ( MEMORY[0x42E22B7C] == 1 )
            {
              LODWORD(ptrToDbt) = &DBT_Stopping_SIPC_Dual_PDP_support__;
              HIDWORD(ptrToDbt) = 0x40522;
              palLog(&ptrToDbt, 0xFECDBA98);
              v24 = 0;
LABEL_72:
              sm_SetDualPdpActivation(v44, v24);
              goto LABEL_16;
            }
          }
          else
          {
            v23 = MEMORY[0x42E22B7C] == 1;
          }
          if ( !v23 )
          {
            goto LABEL_16;
          }
          LODWORD(ptrToDbt) = &DBT_Setting_SIPC_Dual_PDP_support__;
          HIDWORD(ptrToDbt) = 0x40522;
          palLog(&ptrToDbt, 0xFECDBA98);
          v24 = 1;
          goto LABEL_72;
        }
      }
      else
      {
        v46 = 0;
      }
      sm_SetPdpAddressLength(v44, 0, 1);
      goto LABEL_47;
    }
  }
  v4 = 0x60;
LABEL_16:
  smGetNsApi(v44, &v52, 1);
  if ( v4 )
  {
    if ( v4 == 0x25 )
    {
      sm_SetSmCause(v44, 0x25);
      LODWORD(ptrToDbt) = &DBT_Deactivating_PDP_due_to_Cause____s_in_sm_DecodePdpActivationAcceptMsg_;
      HIDWORD(ptrToDbt) = 0x40522;
      v28 = display_some_error_0(0x25u);
      palLog(&ptrToDbt, v28, 0xFECDBA98);
      sm_SetDeactivationCause(v44, 0);
      sm_GetPrimaryNsapi(v44, &v53);
      if ( v53 == 0xFF )
      {
        sm_MarkAssociateInstsToTearDown(v44);
        sm_SetFlag(v44, 8);
      }
      else
      {
        sm_ClearFlag(v44, 8);
      }
      smGetFlag(v44, 8, &v56);
      sub_41555738(v44, 0x25, v56);
      sm_GetPrimaryNsapi(v44, &v53);
      sm_SetState(v44, 4);
      v29 = sm_SetDeactivationCause(v44, 1);
      v30 = sm_get_ptr_0(v29);
      if ( v30 == 2 )
      {
        sm_send_msg(v44, v52, 0);
        sm_StartTimer(v44, 3);
      }
      else if ( sm_get_ptr_0(v30) == 1 )
      {
        sm_send_msg_0(v44, v52, 0);
      }
    }
    else
    {
      sm_SetSmCause(v44, v4);
      LODWORD(ptrToDbt) = &DBT_Sending_Status_MSG_due_to_Cause____s_in_sm_DecodePdpActivationAcceptMsg_;
      HIDWORD(ptrToDbt) = 0x40522;
      v31 = display_some_error_0(v4);
      palLog(&ptrToDbt, v31, 0xFECDBA98);
      uninteresting_62(v44, v4, v32, v33);
    }
  }
  else
  {
    sm_SetState(v44, 3);
    v25 = sm_SetFlag(v44, 0x40);
    v26 = sub_4155CB14(v25);
    if ( v26 == 9 )
    {
      v26 = sm_SetIsGlobalDuringPdpFlag(v44, 1);
    }
    if ( sm_get_ptr_0(v26) == 1 )
    {
      sm_GetActivationCause(v44, &v57);
      sm_SendMmcSmRegInfoUpdIndMsg(v44);
      if ( sub_40CFF59E(v27) == 1 && MEMORY[0x42E22B7C] == 1 && v0 == 0xFF )
      {
        sm_SetDualPdpActivation(v44, 0);
      }
      sm_SendSnSmPdpActivationIndMsg(v44, v57);
    }
    else
    {
      smGetFlag(v44, 0x80, &v58);
      if ( v58 == 1 )
      {
        sm_ClearFlag(v44, 0xC0);
      }
      LODWORD(ptrToDbt) = &DBT_Sending_Cnf_to_AP_as_well_as_MMC_;
      HIDWORD(ptrToDbt) = 0x40521;
      palLog(&ptrToDbt, 0xFECDBA98);
      sm_SendSmRegPdpActivationCnfMsg(v44, 0);
      sm_SendMmcSmRegInfoUpdIndMsg(v44);
      sm_SendGmmSmPdpStatusUpdReqMsg(v44, 1);
    }
  }
  sm_DisplaySessionData(v44);
}

signed int __fastcall call_memcpy_new_unchecked_size(char a1, int src, unsigned int size)
{
  signed int v3; // r5
  unsigned int size1; // r10
  int v5; // r11
  char v6; // r9
  unsigned int i; // r4
  unsigned int v8; // r3
  int v10; // r4

  v3 = 0;
  size1 = size;
  v5 = src;
  v6 = a1;
  for ( i = 0; return_0_0() > i; i = (unsigned __int8)(i + 1) )
  {
    if ( isequal(v5, 0x66 * i + 0x42C0DA7D, size1) == 1 )
    {
      v3 = 1;
      *(_BYTE *)(0x66 * i + 0x42C0DAE1) = v6;
      return v3;
    }
  }
  while ( return_0_0() > v8 )
  {
    if ( !*(_BYTE *)(0x66 * v8 + 0x42C0DA7C) )
    {
      v10 = 0x33 * v8;
      *(_BYTE *)(0x66 * v8 + 0x42C0DAE1) = v6;
	  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      memcpy_new(0x66 * v8 + 0x42C0DA7D, v5, size1);
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      *(_BYTE *)(2 * v10 + 0x42C0DA7C) = size1;
      return v3;
    }
  }
  return v3;
}
~~~


# void sm_decode_SM______RADIO_MSG__PDP_ACTIVATION_REJECT(), void sm_decode_SM______RADIO_MSG__SEC_PDP_ACTIVATION_REJECT(), void sm_decode_SM______RADIO_MSG__PDP_MODIFICATION_REJECT()
## similar to the above, all calls call_memcpy_new_unchecked_size_1() using the value returned from sm_getApnLength() without checking
~~~
void __fastcall call_memcpy_new_unchecked_size_1(int a1, int src_1, int size, int a4)
{
  char v4; // r11
  signed int v5; // r7
  int v6; // r5
  int size_1; // r6
  __int64 v8; // r0
  int v9; // r3
  unsigned int i; // r4
  __int64 v11; // r0
  unsigned int j; // r4
  unsigned __int8 *v13; // r3
  int v14; // r12
  _BYTE *v15; // r1
  int v16; // [sp+0h] [bp-40h]
  __int64 v17; // [sp+4h] [bp-3Ch]
  int v18; // [sp+Ch] [bp-34h]
  int src; // [sp+10h] [bp-30h]
  int v20; // [sp+14h] [bp-2Ch]
  int v21; // [sp+18h] [bp-28h]

  v18 = a1;
  src = src_1;
  v20 = size;
  v21 = a4;
  v4 = a1;
  v5 = 0;
  v6 = a4;
  size_1 = size;
  HIDWORD(v8) = 0x40521;
  v9 = a1;
  LODWORD(v8) = &DBT_sm_SetSmT3396Data_timer__d_leng__d_cause__d_;
  v17 = v8;
  v16 = 0xFECDBA98;
  palLog(&v17, v6, size, v9, 0xFECDBA98, &DBT_sm_SetSmT3396Data_timer__d_leng__d_cause__d_, 0x40521, v18);
  for ( i = 0; ; i = (unsigned __int8)(i + 1) )
  {
    if ( return_0_0() <= i )
    {
      goto LABEL_11;
    }
    if ( isequal(src, 0x67 * (signed __int16)i + 0x42C0DEE5, size_1) == 1 && size_1 )
    {
      break;
    }
  }
  if ( *(unsigned __int8 *)(0x67 * (signed __int16)i + 0x42C0DF49) == 0xFF )
  {
    *(_BYTE *)(0x67 * (signed __int16)i + 0x42C0DF49) = v4;
  }
  *(_BYTE *)(0x67 * (signed __int16)i + 0x42C0DF4A) = v6;
  sm_StopTimer(i, 5);
  if ( !v6 )
  {
    sm_StartTimer(i, 5);
  }
  v5 = 1;
LABEL_11:
  HIDWORD(v11) = 0x4052A;
  LODWORD(v11) = &DBT_Result__d_;
  *(_QWORD *)&v16 = v11;
  palLog(&v16, v5, 0xFECDBA98);
  if ( !v5 )
  {
    for ( j = 0; ; j = (unsigned __int8)(j + 1) )
    {
      if ( return_0_0() <= j )
      {
        goto LABEL_16;
      }
      if ( !*(_BYTE *)(v14 + 0xADCC * *v13 + 0x67 * (signed __int16)j + 0xA868) )
      {
        break;
      }
    }
    v15 = (_BYTE *)(0x67 * (signed __int16)j + v14 + 0xADCC * *v13);
    if ( (unsigned __int8)v15[0xA8CD] == 0xFF )
    {
      v15[0xA8CD] = v4;
    }
    v15[0xA8CE] = v6;
    v15[0xA868] = size_1;
    if ( size_1 )
    {
      memcpy_new((int)(v15 + 0xA869), src, size_1);
    }
    if ( !v6 )
    {
      sm_StopTimer(j, 5);
      sm_StartTimer(j, 5);
    }
  }
LABEL_16:
  JUMPOUT(locret_40CF9022);
}
~~~

# 
~~~
signed int __fastcall sm_StoreSecReqDetails(unsigned int idx_dst_1, int idx_src_1)
{
  int idx_src; // r7
  int idx_dst; // r6
  signed int v4; // r5
  int apn_size; // r2
  int pdp_type; // r3

  idx_src = idx_src_1;
  idx_dst = idx_dst_1;
  if ( return0_5(idx_dst_1) <= idx_dst_1 )
  {
    logInvalidPdpInstance(idx_dst, 0x4025541D);
    v4 = 0;
  }
  else
  {
    v4 = 1;
    sm_StoreNegotiatedQosData(idx_dst, MEMORY[0x10], 0);
    sm_StoreMinimumQosData(idx_dst, MEMORY[0x14]);
    sm_SetNsapi(idx_dst, MEMORY[0], 0);
    sm_SetLlcSapi(idx_dst, MEMORY[4], 0);
    sm_SetPrimaryNsapi(idx_dst, MEMORY[3]);
    sm_SetSndcpBufferOverheadLength(idx_dst, MEMORY[6]);
    sm_SetSndcpSndcpBufferStartOffset(idx_dst, MEMORY[7]);
    sm_SetClientMailBoxId(idx_dst, MEMORY[8]);
    call_memcpy_new_unchecked_size_2(idx_dst, idx_src, apn_size, pdp_type);
    sm_SetTrafficFlowTemplateLength(idx_dst, MEMORY[5]);
    if ( MEMORY[5] )
    {
      sm_SetTrafficFlowTemplate(idx_dst, MEMORY[0xC]);
    }
    sm_BuildNSetProtConfigOpts(idx_dst, 0x1A, 0);
  }
  return v4;
}

void __fastcall call_memcpy_new_unchecked_size_2(int idx_dst_1, int idx_src, int apn_size_1, int pdptype_1)
{
  int idx_src_1; // r5
  int idx_dst; // r4
  int v6; // r3
  int dst; // r4
  int src; // r5
  int pdpaddress_size; // [sp+0h] [bp-20h]
  int apn_size; // [sp+4h] [bp-1Ch]
  int pdptype; // [sp+8h] [bp-18h]

  pdpaddress_size = idx_src;
  apn_size = apn_size_1;
  pdptype = pdptype_1;
  idx_src_1 = idx_src;
  idx_dst = idx_dst_1;
  sm_GetPdpType(idx_src, &pdptype, 1);
  sm_SetPdpType(idx_dst, pdptype, 1);
  sm_GetPdpAddressLength(idx_src_1, &pdpaddress_size, 1);
  sm_SetPdpAddressLength(idx_dst, pdpaddress_size, 1);
  sm_getApnLength(idx_src_1, &apn_size, 1);
  sm_SetApnLength(idx_dst, apn_size, 1, v6);
  dst = 0x1D7 * idx_dst;
  src = 0x1D7 * idx_src_1;
  memcpy_new(8 * dst + 0x42C038AA, 8 * src + 0x42C038AA, pdpaddress_size);
  memcpy_new(8 * dst + 0x42C038BF, 8 * src + 0x42C038BF, apn_size);
}
~~~
