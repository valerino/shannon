#include <idc.idc>

static processDbt(ea) {
	// DBT
	auto markerDbt = 0x3a544244;

	// sizeof (DBTStruct)
	auto dbtSize = 0x1c;
	auto addr = Dword(ea);
	if (addr != markerDbt) {
		// no dbt, advance a dword
		return 4;
	}

	// get address of the string message
	auto addrMsg = Dword(ea + 0x10);

	// get the string itself
	auto str = GetString(addrMsg, -1, ASCSTR_C);
	
	// normalize string to make a newname (keep alphanum characters, replace everything else with '_')
	// this may be done automatically with MakeNameEx(ea, str, SN_NOCHECK) probably ....
	auto len = strlen(str);
	auto i = 0;
	auto c = 0;
	for (i=0; i < len; i++) {
		c = ord(str[i]);
		if ((c >= 0x61 && c <= 0x7a ) ||
				(c >= 0x41 && c <= 0x5a ) ||
				(c >= 0x30 && c <= 0x39) ) {
			// ok, alphanum
		}
		else {
			// replace with permitted char
			str[i] = '_';
		}
	}
	
	// done, rename
	auto newName = "_DBT_" + str;
	auto res = 0;
	auto str_idx = 0;
	while (res == 0) {
		res = MakeNameEx(ea, newName, SN_AUTO);
		if (res == 0) {
			// retry with a different name
			str_idx++;
			newName = "_DBT_" + str + "_" + ltoa(str_idx,10);
			//Message(". failed, retrying with %s\n", newName);
		}
		else {
			Message(". renamed DBT at 0x%x, DBT->msg at 0x%x, newName=%s\n", ea, addrMsg, newName);
		} 
	}
	return dbtSize;
}


static main() {
	auto ea = ScreenEA();
	auto begin = SegStart(ea);
	auto end = SegEnd(ea);
	auto test = 0;
	ea = begin;
	Message (". Start, start=0x%x, end=0x%x\n", begin, end);

	if (test) {
		// test code with single address
		ea = 0x41b0ecb4;
		processDbt(ea);
	}
	else {
		ea = begin;
		// loop for all addres space
		while (ea < end) {
			// process dbt structure (if found)
			auto advance = processDbt(ea);
			ea = ea + advance;
		}
	}
	Message(". Done!\n");
}

