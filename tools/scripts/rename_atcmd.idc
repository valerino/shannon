#include <idc.idc>
static normalizeString(str) {
	auto len = strlen(str);
	auto i = 0;
	auto c = 0;
	for (i=0; i < len; i++) {
		c = ord(str[i]);
		if ((c >= 0x61 && c <= 0x7a ) ||
				(c >= 0x41 && c <= 0x5a ) ||
				(c >= 0x30 && c <= 0x39) ) {
			// ok, alphanum
		}
		else {
			// replace with permitted char
			str[i] = '_';
		}
	}
	return str;
}

static process(ea, structSize, addressName, ptrName) {
	auto dw = Dword(ea);
	auto nameoffset = Dword(ea + 0x10);
	auto rspoffset = Dword(ea + 0x18);
	auto queoffset = Dword(ea + 0x20);
	auto msghandlerstr = normalizeString(GetString(nameoffset, -1, ASCSTR_C)); 
	auto rsphandlerstr = normalizeString(GetString(rspoffset, -1, ASCSTR_C)); 
	auto questr = normalizeString(GetString(queoffset, -1, ASCSTR_C));
	auto namestr = addressName + msghandlerstr;
	
	auto address = Dword(ea + 0xc);
	auto addressrsp = Dword(ea + 0x14);
	auto addressque = Dword(ea + 0x1c);
	auto ptrstring = ptrName + msghandlerstr;
	auto ptrrspstring = ptrName + rsphandlerstr;
	auto ptrquestring = ptrName + questr;
	Message("ea=0x%x, name=%s, address=0x%x\n", ea, namestr, address);
	
	auto res = 0;
	while (res == 0) {
		// rename struct
		res = MakeNameEx(ea, namestr, SN_NOCHECK);
		if (res == 0) {
			namestr = namestr + "_";
		} 

		// rename ptr
		if (address != 0) { 
			res = MakeNameEx(address - 1, ptrstring, SN_NOCHECK);
			if (res == 0) {
				ptrstring = ptrstring + "_";
			} 
		}
		// rename ptr
		if (addressrsp != 0) { 
		res = MakeNameEx(addressrsp - 1, ptrrspstring, SN_NOCHECK);
			if (res == 0) {
				ptrrspstring = ptrrspstring + "_";
			} 
		}
		
		// rename ptr
		if (addressque != 0) {
			res = MakeNameEx(addressque - 1, ptrquestring, SN_NOCHECK);
			if (res == 0) {
				ptrquestring = ptrquestring + "_";
			}
		}
	}	
	return structSize;
}

static main() {
	// ask informations
	auto begin = 0x419dd2d8; // 0x419d4c38; // AskAddr(SelStart(), "enter start address");
	auto end = 0x419df6a4; // AskAddr(SelEnd(), "enter end address");
    auto addressName = AskStr("Handler_", "enter prefix for address generated name");	
	auto ptrName = AskStr("_handle_", "enter prefix for functionptr generated name");
	auto structSize=0x30;
	Message (". Start, start=0x%x, end=0x%x, structSize=0x%x, addressPrefix=%s, ptrPrefix=%s\n", begin, end, structSize, addressName, ptrName);

	// loop for all addres space
	auto ea = begin;
	while (ea < end) {
		auto advance = process(ea, structSize, addressName, ptrName);
		ea = ea + advance;
	}
	Message(". Done!\n");
}

