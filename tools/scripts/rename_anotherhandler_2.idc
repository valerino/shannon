#include <idc.idc>
static normalizeString(str) {
	auto len = strlen(str);
	auto i = 0;
	auto c = 0;
	for (i=0; i < len; i++) {
		c = ord(str[i]);
		if ((c >= 0x61 && c <= 0x7a ) ||
				(c >= 0x41 && c <= 0x5a ) ||
				(c >= 0x30 && c <= 0x39) ) {
			// ok, alphanum
		}
		else {
			// replace with permitted char
			str[i] = '_';
		}
	}
	return str;
}

static process(ea, structSize, addressName, ptrName) {
	auto nameoffset = Dword(ea);
	auto msghandlerstr = normalizeString(GetString(nameoffset, -1, ASCSTR_C)); 
	auto namestr = addressName + msghandlerstr;
	auto address = Dword(ea + 0x8);
	auto ptrstring = ptrName + msghandlerstr;
	Message("ea=0x%x, name=%s, address=0x%x\n", ea, namestr, address);
	/*auto id = GetStrucIdByName("VAnotherHandlerType");
	DelStruc(id);
	auto newid= AddStrucEx(-1, "VAnotherHandlerType",0 );	
	AddStrucMember(newid, "str", 0, FF_DWRD|FF_1OFF, -1, 4);
	AddStrucMember(newid, "unk1", 4, FF_DWRD|FF_DATA, -1, 4);
	AddStrucMember(newid, "ptr", 8, FF_DWORD|FF_1OFF, -1, 4);
	*/
	MakeStructEx(ea, 0xc, "VAnotherHandlerType");
	auto res = 0;
	while (res == 0) {
		// rename struct
		res = MakeNameEx(ea, namestr, SN_NOCHECK);
		if (res == 0) {
			namestr = namestr + "_";
		} 
			
		if (address != 0) {
			// rename ptr
			res = MakeNameEx(address - 1, ptrstring, SN_NOCHECK);
			if (res == 0) {
				ptrstring = ptrstring + "_";
			} 
		}
	}

	return structSize;
}

static main() {
	// ask informations
	auto begin = AskAddr(SelStart(), "enter start address");
	auto end = AskAddr(SelEnd(), "enter end address");
    auto addressName = "Unknown"; // AskStr("Handler_", "enter prefix for address generated name");	
	auto ptrName = "unknown_handle_"; // AskStr("_handle_", "enter prefix for functionptr generated name");
	auto structSize=0xc;
	Message (". Start, start=0x%x, end=0x%x, structSize=0x%x, addressPrefix=%s, ptrPrefix=%s\n", begin, end, structSize, addressName, ptrName);

	// loop for all addres space
	auto ea = begin;
	while (ea < end) {
		auto advance = process(ea, structSize, addressName, ptrName);
		ea = ea + advance;
	}
	Message(". Done!\n");
}

