#include <idc.idc>
static normalizeString(str) {
	auto len = strlen(str);
	auto i = 0;
	auto c = 0;
	for (i=0; i < len; i++) {
		c = ord(str[i]);
		if ((c >= 0x61 && c <= 0x7a ) ||
				(c >= 0x41 && c <= 0x5a ) ||
				(c >= 0x30 && c <= 0x39) ) {
			// ok, alphanum
		}
		else {
			// replace with permitted char
			str[i] = '_';
		}
	}
	return str;
}

static process(ea) {
	auto dw = Dword(ea);
	auto nameoffset = Dword(ea + 0x0c);
	auto prefix = "SMSHandler_";
	auto namestr = prefix + normalizeString(GetString(nameoffset, -1, ASCSTR_C));
	auto address = Dword(ea);
	Message("ea=0x%x, name=%s, address=0x%x\n", ea, namestr, address);
	
	auto res = 0;
	while (res == 0) {
		res = MakeNameEx(ea, namestr, SN_NOCHECK);
		if (res == 0) {
			namestr = namestr + "_";
		} 
	}	
	return 0x10;
}

static main() {
	auto ea = ScreenEA();
	auto test = 0;
	
	//auto testAddr = 0x404C5810;
	//auto testAddr = 0x404c5b44;
	auto testAddr = 0x404c5bf8;
	auto begin = testAddr;
	//auto end = 0x404c5ae0;
//	auto end = 0x404c5ba8;
	auto end = 0x404c5c48;	
	Message (". Start, start=0x%x, end=0x%x\n", begin, end);

	if (test) {
		// test code with single address
		ea = testAddr;
		process(ea);
	}
	else {
		ea = testAddr;
		// loop for all addres space
		while (ea < end) {
			auto advance = process(ea);
			ea = ea + advance;
		}
	}
	Message(". Done!\n");
}

