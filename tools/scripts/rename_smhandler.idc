#include <idc.idc>
static normalizeString(str) {
	auto len = strlen(str);
	auto i = 0;
	auto c = 0;
	for (i=0; i < len; i++) {
		c = ord(str[i]);
		if ((c >= 0x61 && c <= 0x7a ) ||
				(c >= 0x41 && c <= 0x5a ) ||
				(c >= 0x30 && c <= 0x39) ) {
			// ok, alphanum
		}
		else {
			// replace with permitted char
			str[i] = '_';
		}
	}
	return str;
}

static process(ea) {
	auto dw = Dword(ea);
	auto nameoffset = Dword(ea + 0x0c);
	auto prefix = "SMHandler_";
	auto msghandlerstr = normalizeString(GetString(nameoffset, -1, ASCSTR_C)); 
	auto namestr = prefix + msghandlerstr;
	auto address = Dword(ea);
	auto ptrstring = "sm_decode_" + msghandlerstr;
	Message("ea=0x%x, name=%s, address=0x%x\n", ea, namestr, address);
	
	auto res = 0;
	while (res == 0) {
		// rename struct
		res = MakeNameEx(ea, namestr, SN_NOCHECK);
		if (res == 0) {
			namestr = namestr + "_";
		} 

		// rename ptr
		res = MakeNameEx(address - 1, ptrstring, SN_NOCHECK);
		if (res == 0) {
			ptrstring = ptrstring + "_";
		} 
	}	
	return 0x10;
}

static main() {
	auto ea = ScreenEA();
	auto test = 0;
	
	auto testAddr = 0x40254798;

	auto begin = testAddr;
	auto end = 0x40254a38;
	Message (". Start, start=0x%x, end=0x%x\n", begin, end);

	if (test) {
		// test code with single address
		ea = testAddr;
		process(ea);
	}
	else {
		ea = testAddr;
		// loop for all addres space
		while (ea < end) {
			auto advance = process(ea);
			ea = ea + advance;
		}
	}
	Message(". Done!\n");
}

