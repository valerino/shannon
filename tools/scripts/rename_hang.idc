#include <idc.idc>

static process(ea) {
	auto marker = 0x46c04778;
	auto dw = Dword(ea);
	if (dw != marker) {
		// no dbt, advance a dword
		return 4;
	}
	auto str = "armSwitch";
	auto res = 0;
	
	while (res == 0) {
		Message(". found armSwitch at 0x%x\n", ea);
		res = MakeNameEx(ea, str, SN_AUTO);
		if (res == 0) {
			str = str + "_";
		}
	}
	return 4;
}

static main() {
	auto ea = ScreenEA();
	auto begin = SegStart(ea);
	auto end = SegEnd(ea);
	auto test = 0;
	auto testAddr = 0x40ebe05c;
	Message (". Start, start=0x%x, end=0x%x\n", begin, end);

	if (test) {
		// test code with single address
		ea = testAddr;
		process(ea);
	}
	else {
		ea = begin;
		ea = 0x40ebe0bc;
		// loop for all addres space
		while (ea < end) {
			auto advance = process(ea);
			ea = ea + advance;
		}
	}
	Message(". Done!\n");
}

