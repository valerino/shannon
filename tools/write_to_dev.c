#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>

void hexdump(unsigned char *buffer, unsigned long index, unsigned long width, unsigned long total) {
	unsigned long i;
	printf("0x%08x : ", total);

	for (i=0;i<index;i++) {
		printf("%02x ",buffer[i]);
	}
	unsigned long long spacer;
	for (spacer=index;spacer<width;spacer++) {
		printf("   ");
	}

	printf(": ");
	for (i=0;i<index;i++) {
		if (!isprint(buffer[i])) {
			printf(".");
		}
		else {
			printf("%c",buffer[i]);
		}
	}
	printf("\n");
}

int hexdump_file(char* path, unsigned long offset) {
	char ch;
	unsigned long f_index=0;
	unsigned long bb_index=0;
	int width = 16;
	unsigned char *byte_buffer = malloc(width);
	if (byte_buffer == NULL)
	{
		printf("Could not allocate memory for byte_buffer\n");
		return 1;
	}

	FILE* infile = fopen(path, "rb");
	fseek(infile, offset, SEEK_END);
	long stop = ftell(infile) - 1;
	rewind(infile);
	long start = 0;
	unsigned long total = offset;
	while (!feof(infile))
	{
		ch = getc(infile);
		if ((f_index >= start)&&(f_index <= stop))
		{
			byte_buffer[bb_index] = ch;
			bb_index++;
		}
		if (bb_index >= width)
		{
			hexdump(byte_buffer,bb_index,width, total);
			bb_index=0;
			total += width;
		}
		f_index++;
	}
	if (bb_index) {
		hexdump(byte_buffer,bb_index,width, total);
	}
	fclose(infile);
	free(byte_buffer);
	return 0;
}

int read_device(char* dev, char* dest, int continuos) {
	char* buf = (char*)calloc(1,0x4000);
	unlink(dest);
	unsigned long offset = 0;
	while (1) {
		size_t r;
		FILE* fdump = fopen(dest, "a+b");
		if (!fdump) {
			printf("[x] cannot open destination file %s\n", dest);
			return 1;
		}
		int f = open(dev, O_RDONLY|O_NOCTTY);
		if (f == -1) {
			printf("[x] cannot open device %s\n", dev);
			fclose(fdump);
			return 1;
		}

		r = read(f,buf,0x4000);
		if (r == 0) {
			fclose(fdump);
			close(f);
			continue;
		}
		printf("[-] read %d(0x%x) bytes from %s to %s, offset=%d(0x%x) ....\n", r, r, dev, dest, offset, offset);
		fwrite(buf,r,1,fdump);
		if (!continuos) {
			// only perform a single read
			fclose(fdump);
			close(f);
			hexdump_file(dest, 0);
			break;
		}
		fclose(fdump);
		close(f);
		hexdump_file(dest, offset);
		offset+=r;
	}
	return 0;
}

int write_device(char* dev, char* from) {
	FILE* f = fopen(from,"rb");
	if (!f) {
		printf("[x] cannot open file %s\n", from);
		return 1;
	}
	fseek(f, 0, SEEK_END);
	long size = ftell(f);
	rewind(f);
	char* mem = (char*)calloc(1,size + 1);
	fread(mem,size,1,f);
	fclose(f);

	printf("[-] writing %d(0x%x) bytes from file %s to device %s ...\n", size, size, from, dev);
	f = fopen(dev,"w+b");
	if (!f) {
		printf("[x] cannot open device %s\n", dev);
		return 1;
	}
	fwrite(mem,size,1,f);
	fclose(f);
	return 0;
}

int fill_device(char* dev, int c, int size) {
	printf("[-] writing %d(0x%x) '%c'(0x%x) to device %s ...\n", size, size, c, c,  dev);
	FILE* f = fopen(dev,"w+b");
	if (!f) {
		printf("[x] cannot open device %s\n", dev);
		return 1;
	}
	char* buf = (char*)calloc(1,size);
	memset(buf,c,size);
	fwrite(buf,size,1,f);
	fclose(f);
	return 0;
}

void print_usage(char** argv) {
	printf("usage:\n\t%s r <device> <dest> (read from device to destination file, once)\n", argv[0]);
	printf("\t%s rr <device> <dest> (read from device to destination file, continuos (stop with ctrl-c))\n", argv[0]);
	printf("\t%s w <device> <file> (write file to device)\n", argv[0]);
	printf("\t%s f <device> <char|0xchar> <size> (fill device with size chars)\n", argv[0]);
}

int main (int argc, char** argv) {
	if (argc < 4) {
		print_usage(argv);
		exit(1);
	}
	int res = 0;
	char* op = argv[1];
	char* dev = argv[2];
	char* param1=argv[3];
	char* param2=argv[4];
	if (*op=='w') {
		res = write_device(dev, param1);
	}
	else if (*op =='f') {
		if (argc != 5) {
			print_usage(argv);
			exit(1);
		}
		int n = *param1;
		if (param1[0] == '0' && param1[1] == 'x') {
			n = strtol(param1, NULL, 0);
		}
		res = fill_device(dev,n,atoi(param2));
	}
	else if (*op == 'r') {
		int continuos = (op[1] == 'r');
		res = read_device(dev,param1, continuos);
	}
	else {
		print_usage(argv);
		exit(1);
	}
	return res;
}

