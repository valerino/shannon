"""
needs structures.idc loaded!

convert starting from selected address to MsgHandler and rename the relevant function pointers

!! needs to set begin and end addresses in main !!

TODO: the script resolves and renames everything but sometimes fail to create the structure itself, needs to be fixed by hand
TODO: input start and end address
"""

import idc
import string

def processHandler(ea, task):
    sz = get_struc_size(get_struc_id('MsgHandler'))
    print '[-] creating MsgHandler at 0x%x, size=%d' % (ea, sz)

    # set structure
    MakeStruct(ea, 'MsgHandler')

    # set function names
    msgName = GetString(Dword(ea + 0xc), -1, ASCSTR_C)
    msgFunc = Dword(ea)
    if msgFunc is not 0:
        name = '%s_%s' % (task, msgName) 
        print '[-] renaming msg handler routine ptr at 0x%x to %s' % (msgFunc, name)
        set_name(msgFunc - 1, name, SN_AUTO|SN_NOCHECK)

    return sz
    
def main():
    # cc handlers
    begin = 0x401f84f0 
    end = 0x401f89b0
    task = 'cc'
    ea = begin
    while ea < end:
        advance = processHandler(ea, task)
        ea += advance

    print '[-] DONE !'

if __name__== "__main__":
  main()


