#!/usr/bin/env sh
# splits CP modem.bin into BOOT and MAIN
# G930FXXU1APD1 SM-G930F_1_20160408142840_j6prs1woa7_fac
rm BOOT MAIN
echo "[x] creating BOOT....."
dd skip=512 if=./modem.bin of=./BOOT count=7860 bs=1
echo "[x] calculating BOOT crc32....."
crc32 ./BOOT
echo "[x] creating MAIN....."
dd skip=8384 if=./modem.bin of=./MAIN count=38361252 bs=1
crc32 ./MAIN
echo "[x] calculating MAIN crc32....."
echo "[x] done! crc32 must be BOOT=0xdd62551, MAIN=0x1c5cc6b1"

