"""
needs structures.idc loaded!

convert starting from selected address to TaskStruct and rename the relevant function pointers

!! needs to set begin and end addresses in main, task table can be found easily by looking for references to 'Default' task !!

TODO: the script resolves like 90% of the function pointers, but misses something (probably due to arm/thumb switch). once the script is done, the task table must be inspected and the missing stuff like unk_etc... instead of proper functions must be fixed by hand

"""

import idc
import string

def processTask(ea):
    sz = get_struc_size(get_struc_id('TaskStruct'))
    print '[-] creating TaskStruct at 0x%x, size=%d' % (ea, sz)

    # set structure
    MakeStruct(ea, 'TaskStruct')

    # set function names
    taskName = GetString(Dword(ea + 0x14), -1, ASCSTR_C)
    taskFunc = Dword(ea + 0x20)
    unkFunc = Dword(ea + 0x24) 
    if taskFunc is not 0:
        name = 'OSTask_%s' % taskName 
        print '[-] renaming taskroutine ptr at 0x%x to %s' % (taskFunc, name)
        set_name(taskFunc - 1, name, SN_AUTO|SN_NOCHECK)

    if unkFunc is not 0:
        name = 'OSTask_unk_%s' % taskName 
        print '[-] renaming unk_taskroutine ptr at 0x%x to %s' % (unkFunc, name) 
        set_name(unkFunc - 1, name, SN_AUTO|SN_NOCHECK)

    return sz
    
def main():
    begin = 0x415d5788
    end = 0x415dbc98
    ea = begin
    while ea < end:
        advance = processTask(ea)
        ea += advance
    print '[-] DONE !'

if __name__== "__main__":
  main()


