#!/usr/bin/env python3
import binascii
import argparse
import sys
import struct
import hexdump
from capstone import *

# from dump_registers()
REG_STRUCT_ADDRESS = 0x434D2F04
DUMP_BASE = 0X40000000

DUMP_INFO = {
        'line':0x434D2F04, 
        'file':0x434D2F08,
        'error':0x434D2F0C
        }
        
DUMP_REGS = {
        'R0':0x434D3118,
        'R1':0x434D311C,
        'R2':0x434D3120,
        'R3':0x434D3124,
        'R4':0x434D3128,
        'R5':0x434D312C,
        'R6':0x434D3130,
        'R7':0x434D3134,
        'R8':0x434D3138,
        'R9':0x434D313C,
        'R10':0x434D3140,
        'R11':0x434D3144,
        'R12':0x434D3148,
        'PC':0x434D314C,
        'CPSR':0x434D3150,
        'SP_svc':0x434D3160,
        'LR_svc':0x434D3164,
        'SP_usr':0x434D3154,
        'LR_usr':0x434D3158,
        'SPSR_abt':0x434D3168,
        'SP_abt':0x434D316C,
        'LR_abt':0x434D3170,
        'SPSR_und':0x434D3174,
        'SP_und':0x434D3178,
        'LR_und)':0x434D317C,
        'SPSR_irq':0x434D3180,
        'SP_irq':0x434D3184,
        'LR_irq':0x434D3188
        }
 
DUMP_FILE_NAME = 'memory/cp_f0000000_f7ffffff.bin'

def addr_to_file_offset(addr):
    return (addr - DUMP_BASE)


def read_dump_to_memory(args):
    f = open(args.dumpfile[0], 'rb')
    b = bytearray(f.read())
    f.close()
    return b


def read_dword(dump, addr):
    d = dump[addr_to_file_offset(addr):addr_to_file_offset(addr + 4)] 
    i = struct.unpack('I', d)[0]
    # print ('read dword=%08x' % i)
    return i


def read_string(dump, addr):
    d = addr_to_file_offset(read_dword(dump, addr))
    s = ''
    while dump[d] != 0:
        s += chr(dump[d])
        d+=1
    return s


def print_item(name, fmt, val):
    print (('[-] %s: ' + fmt) % (name, val))


def print_stack_frame(dump, sp, lr):
    bottom = 0x4804000
    i = 0
    cr = 0
    print('\n[+] stack dump following ...')
    print ('\t[-] frame SP=0x%08x, LR=0x%08x' % (sp, lr))
    start_sp = sp
    sp -= (0x80*4)
    while 1:
        val = read_dword(dump, sp)
        if cr == 0:
            # print new line
            print ('\t[0x%08x]:\t0x%08x' % (sp, val), end='')
            cr+=1

        elif cr == 7:
            # print cr and reset lines
            if sp - (4*7)== start_sp:
                marker = ' <=== SP'
            else:
                marker = ''
            print (', 0x%08x %s' % (val, marker))
            cr = 0

        else:
            # print value
            print (', 0x%08x' % (val), end='')
            cr+=1

        if sp == bottom:
            break

        sp += 4
        i+=1

        if i == 0x80*2:
            print('')
            break

def print_dump_info(dump):
    # dump info
    l = list(DUMP_INFO.keys())
    print_item (l[0], '%d', read_dword(dump, DUMP_INFO[l[0]]))
    print_item (l[1], '%s', read_string(dump, DUMP_INFO[l[1]]))
    print_item (l[2], '%s', read_string(dump, DUMP_INFO[l[2]]))

    # registers
    l = list(DUMP_REGS.keys())
    i = 0
    for k in l:
        print_item(l[i], '0x%08x', read_dword(dump, DUMP_REGS[l[i]]))
        i+=1

   
    # stack walk
    sp = read_dword(dump, DUMP_REGS['SP_svc'])
    lr = read_dword(dump, DUMP_REGS['LR_svc'])
    print_stack_frame(dump, sp, lr)
    
    '''
    print ('[-] Starting stack unwind...')
    md = Cs(CS_ARCH_ARM, CS_MODE_ARM)

    # get a slice of code around LR
    lr = addr_to_file_offset(read_dword(dump,DUMP_REGS['LR_svc']))
    print('lr: %x' % lr)
    start = lr - 4
    chunk = dump[start:lr]
    for i in md.disasm(chunk, start + DUMP_BASE):
        print("%x:%s\t\t%s\t%s" %(i.address, binascii.hexlify(i.bytes), i.mnemonic, i.op_str))
    '''

def main():
    parser = argparse.ArgumentParser('parse CP dump')
    parser.add_argument('--dumpfile', nargs=1, default=DUMP_FILE_NAME, help='dump file path')
    args = parser.parse_args()

    # read dump
    dump = read_dump_to_memory(args)
    print_dump_info(dump)
    
    
if __name__ == "__main__":
    main()
