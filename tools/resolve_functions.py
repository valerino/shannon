"""
try to make functions from unexplored data .... use with caution!
!! needs to set begin/end in main(), code_thumb must be -1 for thumb code and 0 for ARM !! 
"""

import idc
import string

def processChunk(ea):
    start = FindUnexplored(ea, idaapi.BIN_SEARCH_FORWARD)
    # should MakeCode() be put here, or after MakeFunction() is successful ?
    # putting it here converts the chunk to code always .....
    MakeCode(start)
    
    # print '[-] found unexplored at 0x%x' % start
    if MakeFunction(start) != 0:	
        end = GetFunctionAttr(start, FUNCATTR_END)
        size = end - start
        print '[-] created function at 0x%x, size=0x%x, next function should be at 0x%x' % (start, size, end)
        return start, end, size
    else:
        # can't convert to function
        return start,0,0
    
def main():
    # set this to 0 for ARM
    code_thumb = -1 
    
    begin = get_screen_ea() + code_thumb
    #begin = get_segm_start(ea)
    end = get_segm_end(begin)
    failures = 0
    max_failures = 1024
    use_max_failures = False
    
    ea = begin
    while ea < end:
        func_start, func_end, size = processChunk(ea)
        if size == 0:
            # print '[x] creating function failed, better to check address 0x%x!' % func_start
            # try skip a dword ....
            ea = func_start + 1
            failures+=1
        else:
            ea = func_end + code_thumb
            failures = 0
         
        if use_max_failures and (failures == max_failures):
            # as a precaution to not screw the db too much, after some failures we break and need
            # to analyze manually...
            print '[x] stopped after too many failures, check between 0x%x and 0x%x' % (func_start - max_failures, func_start)
            Jump(func_start - max_failures)
            break

    print '[-] DONE !'

if __name__== "__main__":
  main()


