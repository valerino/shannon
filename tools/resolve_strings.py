"""
try to batch-convert strings, after locating a string array .... use with caution!
!! works on selected address !! 
"""

import idc
import string
import idautils
import idaapi

# defines the string type
string_type = ASCSTR_C

def processChunk(ea):
    # get bytes at buf
    buf = get_bytes(ea, 4)
    if ord(buf[0]) == 0:
        # for double \0 terminated strings...
        return 0
        
    # check if they're alphanum
    # will fail for strlen < 4, though....
    fail = False
    for c in buf:
        if not c.isalnum() :
            fail = True
            break

        if fail:
            # probably not a string ....
            return -1
        
    # go on, try to make string and return length ....
    s = get_strlit_contents(ea)
    if s == None:
        # something wrong
        return -1
        
    l = len(s)
    print '[-] found string "%s" at 0x%x, length=%d' % (s, ea, l)
    MakeUnknown(ea, l, DELIT_EXPAND|DELIT_DELNAMES)
    idaapi.make_ascii_string(ea, l, string_type)
    return l
        
def main():
    begin = get_screen_ea()
    ea = begin
    while 1:
        size = processChunk(ea)
        if size == -1:
            print '[x] seems not a string at 0x%x' % ea
            Jump(ea)
            break
        else:
            # advance strlen(string) + 1
            ea = ea + size + 1
            
    print '[-] DONE !'

if __name__== "__main__":
  main()



