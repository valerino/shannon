"""
renames DBT structures using their inner string ptr
needs structures.idc loaded!
"""

import idc
import string

def processDbt(ea):
    # DBT
    markerDbt = 0x3a544244;

    # sizeof (DBTStruct)
    dbtSize = get_struc_size(get_struc_id('DBTStruct'))
    addr = Dword(ea);
    if addr != markerDbt:
        # no dbt, advance a dword
        return 4

    # set structure
    MakeStruct(ea, 'DBTStruct')

        # get address of the string message
    addrMsg = Dword(ea + 0x10)

    # get the string itself
    str = GetString(addrMsg, -1, ASCSTR_C);
    if str == None:
        print '[x] string is NULL!'
        return 4
    
    # normalize string replacing invalid characters for name
    l = len(str)
    normalized = ''
    for c in str:
        if c.isalnum():
            normalized += c
        else:
            normalized += '_'
        
    newName = 'DBT_%s' % normalized
    res = 0
    str_idx = 0
    while res == 0:
        res = set_name(ea, newName, SN_AUTO|SN_NOCHECK)
        if res == 0:
            # retry
            str_idx +=1
            newName = 'DBT_%s_%d' % (normalized, str_idx)
            print '[x] retrying with name=%s' % newName
        else:
            print '[-] renamed DBT at 0x%x, DBT->msg at 0x%x, newName=%s\n' % (ea, addrMsg, newName)
    
    return dbtSize

def main():
    ea = get_screen_ea()
    begin = get_segm_start(ea)
    end = get_segm_end(ea)
    
    # set test to true for debugging script
    test = False 
    print '[-] segment start=0x%x, end=0x%x' % (begin, end)
    if test:
        print '[-] ** TEST MODE **'
    else:
        ea = begin
        while ea < end:
            advance = processDbt(ea)
            ea += advance
            
    print '[-] DONE !'

if __name__== "__main__":
  main()


